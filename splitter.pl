#!/usr/bin/env perl

use strict;
use warnings;

use Fcntl;
use Text::CSV;
use Data::Dumper;

open my $contacts_file, "+<:encoding(utf8)", "comptables.csv" or die "[ERROR] Unable to open the file \"comptables.csv\" in read/write mode : $!";

# Allocating the CSV backend.
my $csv = Text::CSV->new({
    sep_char => ";"
}) or die "[ERROR] Unable to allocate Text::CSV : " . Text::CSV->error_diag;

my @contacts_csv = <$contacts_file> 
    and seek $contacts_file, 0, 0 
    or die "[ERROR] Unable to read the file : $!";
}



close $contacts_file 
    or die "[ERROR] Unable to properly close the file \"comptables.csv\" : $!";
