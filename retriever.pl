#!/usr/bin/env perl

use strict;
use warnings;

use Mojo::DOM;
use Text::CSV;
use LWP::Simple;
use Mojo::Collection;

my $base_url = "http://36comptables.com";

my @countries_paths   = ();
my @departments_paths = ();
my @contacts_paths    = ();

# Gets the content of the requested page.
# Parameter #1 : URL
# Return : the content of the page.
sub get_page {
    my $url     = shift;
    my $webpage = undef;
    while ($webpage = get $url) {
	if ($webpage) {
	    last;
        }
    }
    
    return $webpage;
}

# Parses the webpage if its type is directory.
# Parameter #1 : the url of the webpage.
# Return : an array containing the paths to each contry.
sub parse_directory_webpage {
    my $directory_url = shift;
    my $dom           = Mojo::DOM->new(get_page $directory_url);

    # Extraction of the relative paths to the countries pages.
    my @countries_paths = $dom->find("ul.listing a[href]")
                              ->map(attr => "href")
                              ->each;

    return @countries_paths;
}

# Parses the webpage if its type is countries.
# Parameter #1 : the url of the webpage.
# Return : an array containing the paths to each department.
sub parse_countries_webpage {
    my $countries_url = shift;
    my $dom           = Mojo::DOM->new(get_page $countries_url);

    # Extraction of the relative paths to the departments pages.
    my @departments_paths = $dom->find("ul.listing a[href]")
                                ->map(attr => "href")
                                ->each;

    return @departments_paths;
}

# Parses the webpage if its type is departments.
# Parameter #1 : the url of the webpage.
# Return : an array containing the paths to the lists.
sub parse_departments_webpage {
    my $departments_url  = shift;
    my $dom              = Mojo::DOM->new(get_page $departments_url);

    # Departments informations.
    my @departments_urls = ();
    my @contacts_paths   = ();
    my $number_lists     = undef;

    # Extraction of the number of lists per department.
    $dom->find("li.last a[href]")
        ->map(attr => "href")
        ->map(sub {
            m/\/annuaire\/.*\/([\d]*)?/; # The regex catches the page number at the end of the relative url.
            $number_lists = $1;
        });

    # Building URLs. The website doesn't use a number for the first list page.
    if ($number_lists) {
        foreach my $i (1 .. $number_lists) {
            if ($i == 1) {
                push @departments_urls, $departments_url;
            } else {
                push @departments_urls, $departments_url . "/$i";
            }
        }
    } else {
        push @departments_urls, $departments_url;
    }

    # Extraction of the relative paths to each contact.
    foreach my $url (@departments_urls) {
        push @contacts_paths, $dom->parse(get_page $url)
                                  ->find("ul.listing a[href]")
                                  ->map(attr => "href")
                                  ->each;
    }

    return @contacts_paths;
}

# Parses the webpage if its type is contact.
# Parameter #1 : the the url of the webpage.
# Parameter #2 : the current country.
# Return : a hash containing the informations of the contact.
sub parse_contacts_webpage {
    my $contact_url = shift;
    my $dom         = Mojo::DOM->new(get_page $contact_url);

    # Contact informations.
    my @contact_infos = ("") x 62;
    my $first_name    = undef;
    my $last_name     = undef;
    my @last_name     = ();
    my $company_name  = undef;
    my $email         = undef;
    my $phone_number  = undef;
    my $address       = undef;
    my $zipcode       = undef;
    my $city          = undef;
    my $siret         = undef;
    my $manager_name  = undef;
    my $creation_date = undef;

    # Scraping of mail address, SIRET number, manager name and enterprise creation date.
    $dom->find("article.professional p")
        ->map(sub {
            m/(([\w\.-]+)@((:?[\w-]+\.)+)([a-zA-Z]{2,4}))/; # The regex catches the mail address.
            $email = $1 unless !$1;
        });
    $dom->find("article.professional p")
        ->map(sub {
            m/Siret : ([\d]{14})/;                          # The regex catches the SIRET number.
            $siret = $1 unless !$1;
        });
    $dom->find("article.professional p")
        ->map(sub {
            m/rant:  ([\D-][^<>]+)?/;                       # The regex catches the manager name.
            $manager_name = $1 unless !$1;
            if ($manager_name) {
                $manager_name =~ s/^\s([\D])<\/p>$/$1/;     # The regex removes "</p>" tag at the end of $manager_name.
            }
        });
    $dom->find("article.professional p")
        ->map(sub {
            m/.*entreprise: ([\d]+)?/;                      # The regex catches the enterprise creation date.
            $creation_date = $1 unless !$1;
        });

    $company_name = $dom->at("article.professional span[itemprop='name']");
    if ($company_name) {
        $company_name = $company_name->text;
    }
    
    $phone_number = $dom->at("article.professional p span[itemprop='telephone']");
    if ($phone_number) {
        $phone_number = $phone_number->text;
    }
    
    $address      = $dom->at("article.professional p.company-address span");
    if ($address) {
        $address = $address->text;
    }
    
    $zipcode      = $dom->at("article.professional p.company-address span.zipcode");
    if ($zipcode) {
        $zipcode = $zipcode->text;
    }
    
    $city         = $dom->at("article.professional p.company-address span.city-name");
    if ($city) {
        $city = $city->text;
    }

    # Splitting full name in first and last name.
    ($first_name, @last_name) = split / /, $manager_name if $manager_name;
    $last_name                = join  " ", @last_name    if @last_name;

    $contact_infos[1]  = "fournisseur";
    $contact_infos[2]  = $company_name;
    $contact_infos[5]  = $email;
    $contact_infos[6]  = $phone_number;
    $contact_infos[14] = $siret;
    $contact_infos[26] = "Adresse principale";
    $contact_infos[27] = $address;
    $contact_infos[31] = $zipcode;
    $contact_infos[32] = $city;
    $contact_infos[33] = "";
    $contact_infos[37] = $last_name;
    $contact_infos[38] = $first_name;
    $contact_infos[39] = $email;
    $contact_infos[50] = "ECautomation";
    $contact_infos[58] = "expert-comptable";
    $contact_infos[59] = "Actif";

    return @contact_infos;
}

# Writes down contact on the .csv file.
# Parameter #1 : the pointer to the CSV engine.
# Parameter #2 : the contacts file handle.
# Parameter #3 : an array containing each field for one contact.
# Return : nothing.
# TODO : to return an error code if write down fails.
sub write_contact {
    my ($csv, $cf, @contact) = @_;

    # Writing contact informations to the file.
    my $status = $csv->print($cf, \@contact);
    print $cf "\n" or die "[ERROR] Unable to write the current contact on the contacts file : $!";
    
    return $status;
}

# Opening the .csv file.
open my $contacts_file, ">:encoding(utf8)", "comptables.csv" or die "[ERROR] Unable to open the file \"comptables.csv\" in write mode : $!";

# Allocating the CSV backend.
my $csv = Text::CSV->new({
    sep_char => ";"
}) or die "[ERROR] Unable to allocate Text::CSV : " . Text::CSV->error_diag;

@countries_paths = parse_directory_webpage $base_url . "/annuaire";

foreach my $i (@countries_paths) {
    push @departments_paths, parse_countries_webpage $base_url . $i;
    print $i . "\n";
}

foreach my $j (@departments_paths) {
    my @contacts = parse_departments_webpage $base_url . $j;
    push @contacts_paths, @contacts;
    print $j . "\n";
}

foreach my $k (@contacts_paths) {
    my @contact = parse_contacts_webpage $base_url . $k;
    write_contact $csv, $contacts_file, @contact;
    print $k . "\n";
}

close $contacts_file 
    or die "[ERROR] Unable to properly close the contacts file : $!";
